import React from 'react'
import styles from './styles/MainWidget.module.scss'
import { AnimationResolution, Time, TimeLine, AnimationIframe } from './modules'

const MainWidget: React.FC = () => {
    return (
        <div>
            <AnimationResolution />
            <Time />
            <TimeLine />
            <AnimationIframe />
        </div>
    )
}

export default MainWidget