import React from 'react'
import styles from './styles/Time.module.scss'
import { Score, TeamLogos, TeamNames, Timer } from './components'

const Time: React.FC = () => {
    return (
        <div>
            <TeamLogos />
            <TeamNames />
            <Score />
            <Timer />
            < TeamNames />
            <TeamLogos />
        </div>
    )
}

export default Time