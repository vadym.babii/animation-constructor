import React from 'react'
import styles from './styles/Statistics.module.scss'
import { Standings, LineUp, HeadToHead, BetBooster, PlayByPlay, PreviousGames, News, Calendar } from './components/'
const Statistics = () => {
    return (
        <div>
            <Standings />
            <LineUp />
            <HeadToHead />
            <BetBooster />
            <PlayByPlay />
            <PreviousGames />
            <News />
            <Calendar />
        </div>
    )
}

export default Statistics 