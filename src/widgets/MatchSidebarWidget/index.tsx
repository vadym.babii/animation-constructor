import React from 'react'
import styles from './styles/MatchSidebarWidget.module.scss'
import { MatchSelector } from './modules'

const MatchSidebarWidget: React.FC = () => {
  return (
    <div>
      <MatchSelector />
    </div>
  )
}

export default MatchSidebarWidget 