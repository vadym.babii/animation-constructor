import React from 'react'
import styles from './styles/MatchSelector.module.scss'
import { SportsType } from './components'

const MatchSelector: React.FC = () => {
    return (
        <div>
            <SportsType />
        </div>
    )
}

export default MatchSelector