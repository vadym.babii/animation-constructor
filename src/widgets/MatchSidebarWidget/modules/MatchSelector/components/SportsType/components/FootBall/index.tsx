import React from 'react'
import styles from '../../styles/SportsType.module.scss'
import { Date, Event } from './components'

const FootBall: React.FC = () => {
    return (
        <div>
            <Date />
            <Event />
        </div>
    )
}

export default FootBall 