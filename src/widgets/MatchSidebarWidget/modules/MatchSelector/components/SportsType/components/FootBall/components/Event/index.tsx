import React from 'react'
import styles from './styles/Event.module.scss'
import { EventItem } from './components'

const Event: React.FC = () => {
    return (
        <div>
            <EventItem />
        </div>
    )
}

export default Event