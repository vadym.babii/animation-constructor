import React from 'react'
import styles from './styles/OptionsWidget.module.scss';
import { Settings, Style, Language } from './modules/';

const OptionsWidget: React.FC = () => {
  return (
    <div>
      <Settings />
      <Style />
      <Language />
    </div>
  )
}

export default OptionsWidget