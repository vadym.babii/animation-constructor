import React from 'react';
import styles from './styles/Statistics.module.scss'
import { WidgetPosition, StatisticTypes } from './components/';

const Statistics: React.FC = () => {
    return (
        <div>
            <WidgetPosition />
            <StatisticTypes />
        </div>
    )
}

export default Statistics