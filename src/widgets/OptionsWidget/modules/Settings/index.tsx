import React from 'react';
import styles from './styles/Settings.module.scss'
import { DeviceSelection, Animation, Statistics, Header, Timeline } from './components/';

const Settings: React.FC = () => {
  return (
    <div>
      <DeviceSelection />
      < Animation />
      < Statistics />
      < Header />
      <Timeline />
    </div>
  )
}

export default Settings