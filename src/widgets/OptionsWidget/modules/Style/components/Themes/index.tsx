import React from 'react'
import { Theme } from './components';
import { CreateThemeBtn } from './components/elements';
import styles from './styles/Themes.module.scss'

const Themes: React.FC = () => {
    return (
        <div>
            <h2></h2>
            <Theme />
            <CreateThemeBtn />
        </div>
    )
}

export default Themes