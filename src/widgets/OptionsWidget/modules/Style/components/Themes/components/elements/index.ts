export { default as Pallete } from './Pallete';
export { default as CreateThemeBtn } from './CreateThemeBtn';
export { default as PalleteUpdatePopup } from './PalleteUpdatePopup';
export { default as EditThemePopup } from './EditThemePopup';
export { default as CreateThemePopup } from './CreateThemePopup';
