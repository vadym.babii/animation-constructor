import React from 'react';
import styles from '../../styles/Themes.module.scss'
import { Pallete } from '../elements/';

const Theme: React.FC = () => {
    return (
        <div>
            <Pallete />
        </div>
    )
}

export default Theme 