import React from 'react';
import styles from './styles/Style.module.scss'
import { Themes } from './components';

const Style: React.FC = () => {
    return (
        <div><Themes /></div>
    )
}

export default Style