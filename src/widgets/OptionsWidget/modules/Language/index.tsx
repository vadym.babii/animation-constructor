import React from 'react'
import { LanguageSelect } from './components'
import styles from './styles/Language.module.scss'

const Language: React.FC = () => {
    return (
        <div>
            <LanguageSelect />
        </div>
    )
}

export default Language