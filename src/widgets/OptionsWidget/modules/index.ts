export { default as Settings } from './Settings/';
export { default as Style } from './Style/';
export { default as Language } from './Language';