import React from 'react';
import { ConstructorWrapper } from './components';

function App() {
  return (
    <div className="App">
      < ConstructorWrapper />

    </div>
  );
}

export default App;
