import React from 'react';
import styles from './styles/ConstructorWrapper.module.scss';
import { OptionsWidget, MatchSidebarWidget, MainWidget } from '../../widgets/'

const ConstructorWrapper: React.FC = () => {
    return (
        <div>
            <MatchSidebarWidget />
            <MainWidget />
            <OptionsWidget />
        </div>
    )
}

export default ConstructorWrapper